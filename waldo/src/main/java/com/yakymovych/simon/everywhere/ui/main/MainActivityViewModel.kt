package com.yakymovych.simon.everywhere.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.github.ajalt.timberkt.Timber
import com.yakymovych.simon.everywhere.R
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse
import com.yakymovych.simon.everywhere.ui.BaseViewModel
import com.yakymovych.simon.everywhere.ui.main.recyclerview.AlbumDataSourceFactory


class MainActivityViewModel(var albumFactory: AlbumDataSourceFactory) : BaseViewModel() {
    var tasks: LiveData<PagedList<AlbumResponse>>
    var sort: Int = R.id.action_sort_id
    var isResfreshing = MutableLiveData<Boolean>()
    var pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(10)
            .setPageSize(LIMIT).build()

    companion object {
        var LIMIT = 15
    }

    init {
        tasks = LivePagedListBuilder<Int, AlbumResponse>(albumFactory, pagedListConfig).setInitialLoadKey(0).build()
    }

    var refreshListener = OnRefreshListener {
        this.isResfreshing.value = true
        refreshTasks()
    }

    fun refreshTasks() {
        Timber.d { "REFRESHING" }
        albumFactory.dataSource.value?.invalidate()
    }
}