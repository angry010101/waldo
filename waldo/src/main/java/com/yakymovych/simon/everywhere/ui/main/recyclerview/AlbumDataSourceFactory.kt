package com.yakymovych.simon.everywhere.ui.main.recyclerview

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.github.ajalt.timberkt.Timber
import com.yakymovych.simon.everywhere.data.repository.Repository
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse
import javax.inject.Inject

class AlbumDataSourceFactory
@Inject constructor(private var repository: Repository)
    : DataSource.Factory<Int, AlbumResponse>() {

    val dataSource = MutableLiveData<DataSource<Int, AlbumResponse>>()

    override fun create(): DataSource<Int, AlbumResponse> {
        Timber.d { "CREATING DATASOURCE" }
        val ds = AlbumDataSource(repository)
        dataSource.postValue(ds)
        return ds
    }
}