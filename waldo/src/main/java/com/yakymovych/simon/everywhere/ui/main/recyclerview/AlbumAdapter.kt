package com.yakymovych.simon.everywhere.ui.main.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import com.yakymovych.simon.everywhere.R
import com.yakymovych.simon.everywhere.data.AlbumModel
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse
import timber.log.Timber

class AlbumAdapter(val onClick: (GetAlbumQuery.Record) -> Unit) : PagedListAdapter<AlbumResponse, AlbumViewHolder>(AlbumModel.albumDiffCallback) {
    companion object {
        private val TAG = this::class.java.simpleName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : AlbumViewHolder  =
                AlbumViewHolder(LayoutInflater.from(parent.context)
                        .inflate(R.layout.view_holder_task, parent, false))


    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
            Timber.d(TAG, "Binding view holder at position $position")
            holder.task = getItem(position)
    }

}