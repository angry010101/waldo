package com.yakymovych.simon.everywhere.ui.main

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.ajalt.timberkt.Timber
import com.yakymovych.simon.everywhere.R
import com.yakymovych.simon.everywhere.ui.BaseActivity
import com.yakymovych.simon.everywhere.ui.BaseViewModel
import com.yakymovych.simon.everywhere.ui.main.recyclerview.AlbumAdapter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : BaseActivity() {
    override fun getBaseViewModel(): BaseViewModel = viewModel


    @Inject
    lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = AlbumAdapter {
            Timber.d { "SOMETHING PRESSED" }
        }
        viewModel.tasks.observe(this, Observer {
            it?.let {
                Timber.d { "SOMETHING CHANGED" }
                swipeContainer.isRefreshing = false
                adapter.submitList(it)
            }
        })
        viewModel.isResfreshing.observe(this, Observer {
            it?.let {
                swipeContainer.setRefreshing(it)
            }
        })
        swipeContainer.setOnRefreshListener(viewModel.refreshListener)

        tasks_recycler_view.layoutManager =
                LinearLayoutManager(this).apply {
            orientation = RecyclerView.VERTICAL
        }
        tasks_recycler_view.adapter = adapter
    }
}
