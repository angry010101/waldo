package com.yakymovych.simon.everywhere.data.repository

import GetAlbumQuery
import LoginMutation
import android.util.Log
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse
import com.yakymovych.simon.everywhere.data.responses.LoginOrRegisterResponse
import com.yakymovych.simon.everywhere.di.AppModule
import com.yakymovych.simon.everywhere.di.AuthInterceptor
import com.yakymovych.simon.everywhere.di.Named
import com.yakymovych.simon.everywhere.utils.SchedulerProvider
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import javax.inject.Inject
import javax.inject.Singleton

// TODO add additional abstraciton layer. eg. NetworkService
@Singleton
class Repository @Inject constructor(
        @Named("clientWithoutToken") private val client: ApolloClient,
        val schedulerProvider: SchedulerProvider) :
        BaseAuthRepository(AuthInterceptor("")) {


    fun login(email: String, pass: String): Single<LoginOrRegisterResponse> {
        val loginMutation = LoginMutation(email, pass)
        val networkRequest = Single.fromObservable(Rx2Apollo.from(client.mutate(loginMutation)))
        return networkRequest
                .doAfterSuccess { token = it.data?.accountLogin?.auth_token ?: token }
                // TODO separate mappers
                .flatMap {
                    Single.just(LoginOrRegisterResponse().apply {
                        token = it.data?.accountLogin?.auth_token
                        // just for debug purpose
                        // remove in production
                        Log.d("REPOSITORY", "login: TOKEN IS " + it.data?.accountLogin?.auth_token)
                    })
                }
                .compose(schedulerProvider.getSchedulersForSingle())
    }

    fun getTasks(limit: Int, offset: Int): Single<AlbumResponse>? {
        // TODO AS a dependency
        val clientAuth = createClientWithToken()
        val getAlbumQuery = GetAlbumQuery(limit, offset)
        val networkRequest = Single.fromObservable(Rx2Apollo.from(clientAuth.query(getAlbumQuery)))
        return networkRequest
                .flatMap {
                    Single.just(AlbumResponse(it.data?.album?.photos?.records ?: emptyList()))
                }
                .compose(schedulerProvider.getSchedulersForSingle())
    }

    private fun createClientWithToken(): ApolloClient {
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(Interceptor { chain: Interceptor.Chain ->
                    val original: Request = chain.request()
                    val builder: Request.Builder = original
                            .newBuilder()
                            .method(original.method(), original.body())

                    builder.header("Authorization", "Bearer $token")
                    return@Interceptor chain.proceed(builder.build())
                })
                .build()

        return ApolloClient.builder()
                .serverUrl(AppModule.BASE_URL)
                .okHttpClient(httpClient)
                .build()
    }

}