package com.yakymovych.simon.everywhere.ui.main

import com.yakymovych.simon.everywhere.ui.main.recyclerview.AlbumDataSourceFactory
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideViewModel(albumDataSourceFactory: AlbumDataSourceFactory)
            = MainActivityViewModel(albumDataSourceFactory)
}