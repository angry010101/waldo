package com.yakymovych.simon.everywhere.ui.main.recyclerview

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse
import kotlinx.android.synthetic.main.view_holder_task.view.*


class AlbumViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val icon: ImageView

    var task: AlbumResponse? = null
        set(value) {
            Glide.with(icon).clear(icon)
            if (value?.records?.size == 0 || value?.records?.get(0)?.thumbnail_urls?.size == 0) {
                Glide.with(icon).load(android.R.drawable.ic_menu_camera).into(icon)
                return
            }
            value?.records?.get(0)?.thumbnail_urls?.get(0)?.let {
                it.url?.let {
                    Glide.with(icon).load(it).error(android.R.drawable.ic_menu_camera).into(icon)
                }
            }
            field = value
        }

    init {
        icon = view.icon
    }
}