package com.yakymovych.simon.everywhere.ui.main.recyclerview

import androidx.paging.PageKeyedDataSource
import com.yakymovych.simon.everywhere.data.repository.Repository
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse
import io.reactivex.rxkotlin.subscribeBy

// TODO optimize
class AlbumDataSource(private val repository: Repository)
    : PageKeyedDataSource<Int, AlbumResponse>() {

    private var items: MutableList<GetAlbumQuery.Record?> = ArrayList()
    var limit = 10

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, AlbumResponse>) {
        repository.getTasks(limit, 0)?.subscribeBy(
                onSuccess = {
                    this@AlbumDataSource.items.addAll(it.records)
                    callback.onResult(listOf(it!!), 1, 2)
                },
                onError = { throwable ->
                    callback.onResult(arrayListOf(), 0, 0)
                })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, AlbumResponse>) {
        repository.getTasks(limit, limit * params.key)?.subscribeBy(
                onSuccess = {
                    this@AlbumDataSource.items.addAll(it.records)
                    callback.onResult(listOf(it), params.key + 1)
                },
                onError = { throwable ->
                    callback.onResult(arrayListOf(), params.key)
                })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, AlbumResponse>) {

    }
}