package com.yakymovych.simon.everywhere.di

import java.lang.annotation.Documented
import javax.inject.Qualifier


@Qualifier
@Documented
@Retention(AnnotationRetention.RUNTIME)
annotation class Named(
        /** The name.  */
        val value: String = "")