package com.yakymovych.simon.everywhere.di

import android.content.Context
import com.apollographql.apollo.ApolloClient
import com.yakymovych.simon.everywhere.MVVMApplication
import com.yakymovych.simon.everywhere.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import javax.inject.Singleton


@Module
open class AppModule {
    @Singleton
    @Provides
    fun provideContext(application: MVVMApplication): Context = application

    @Provides
    @Singleton
    open fun provideSchedulerProvider() = SchedulerProvider(Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    @Singleton
    @Named("clientWithoutToken")
    fun getApolloClient(): ApolloClient {
        val okHttp = OkHttpClient
                .Builder()
                .build()

        return ApolloClient.builder()
                .serverUrl(BASE_URL)
                .okHttpClient(okHttp)
                .build()
    }

    @Provides
    @Singleton
    @Named("clientWithToken")
    fun getApolloClientWithTokenInterceptor(token: String): ApolloClient {
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(Interceptor { chain: Interceptor.Chain ->
                    val original: Request = chain.request()
                    val builder: Request.Builder = original
                            .newBuilder()
                            .method(original.method(), original.body())

                    builder.header("Authorization", "Bearer $token")
                    return@Interceptor chain.proceed(builder.build())
                })
                .build()

        return ApolloClient.builder()
                .serverUrl(BASE_URL)
                .okHttpClient(httpClient)
                .build()
    }


    companion object {
        const val BASE_URL = "https://core-graphql.dev.waldo.photos/gql"
    }

}