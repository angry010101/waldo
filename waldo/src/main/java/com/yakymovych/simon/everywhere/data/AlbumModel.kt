package com.yakymovych.simon.everywhere.data

import androidx.recyclerview.widget.DiffUtil
import com.yakymovych.simon.everywhere.data.responses.AlbumResponse


data class AlbumModel(
        val title: String
){
    companion object {

        val albumDiffCallback = object : DiffUtil.ItemCallback<AlbumResponse>() {
            override fun areItemsTheSame(oldItem: AlbumResponse, newItem: AlbumResponse): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }

            override fun areContentsTheSame(oldItem: AlbumResponse, newItem: AlbumResponse): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }


        }
    }
}