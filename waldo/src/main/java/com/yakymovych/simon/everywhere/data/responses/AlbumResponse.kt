package com.yakymovych.simon.everywhere.data.responses

data class AlbumResponse(val records: List<GetAlbumQuery.Record?>)